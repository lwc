/******************************************************************************
	dbstree.C 

	Dynamic Binary Search Tree used throughout the entire project.

	A dbsNode applied as a parent class to a specific class will provide
	transparent storage in a dbstree. As a result no duplicates may be
	stored and locating stored data is performed in less than 32 steps.

	Check dbstree.tex for technical information on this tree.

*****************************************************************************/

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "dbstree.h"

//***************************************************************
// A tree of case sensitive strings -- char *Name
//***************************************************************

static char *dbsNodeStrQuery;

int dbsNodeStr.compare (dbsNodeStr *n)
{
	return strcmp (Name, n->Name);
}

int dbsNodeStr.compare ()
{
	return strcmp (Name, dbsNodeStrQuery);
}

dbsNodeStr.dbsNodeStr ()
{
	Name = dbsNodeStrQuery;
}

dbsNodeStr.~dbsNodeStr ()
{
	if (less) delete less;
//	printf ("Deleting %s", Name);
	if (more) delete more;
}

//***************************************************************
// Instantiate implementation of dbsTreeStr
//***************************************************************

dbsTreeTemplate_implementation (dbsNodeStr, dbsTreeStr, 18)

//***************************************************************
// testing
//***************************************************************

int main ()
{
	char inword [100];
	dbsTreeStr wtree;

	while (fgets (inword, 100, stdin)) {
		dbsNodeStrQuery = inword;
		if (!wtree.dbsFind ()) {
			dbsNodeStrQuery = strdup (inword);
			wtree.addself (new dbsNodeStr);
		}
	}
	printf ("ok. %i entries inserted in the dbstree\n", wtree.nnodes);
	delete wtree.root;
	return 0;
}
