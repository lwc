	.file	"gauss.c"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	953267991
	.align 4
.LC2:
	.long	1065353216
	.text
	.p2align 4,,15
.globl gauss
	.type	gauss, @function
gauss:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$108, %esp
	movl	8(%ebp), %edi
	movss	.LC0, %xmm2
	movss	.LC1, %xmm4
	movss	(%edi), %xmm3
	movaps	%xmm3, %xmm0
	andps	%xmm2, %xmm0
	comiss	%xmm4, %xmm0
	jae	.L2
	movss	16(%edi), %xmm0
	andps	%xmm2, %xmm0
	comiss	%xmm4, %xmm0
	jae	.L4
	movl	(%edi), %esi
	movl	%esi, -40(%ebp)
	movl	4(%edi), %ebx
	movl	%ebx, -36(%ebp)
	movl	8(%edi), %ecx
	movl	%ecx, -32(%ebp)
	movl	12(%edi), %edx
	movl	%edx, -28(%ebp)
	movl	32(%edi), %eax
	movl	%esi, 32(%edi)
	movl	%eax, (%edi)
	movl	36(%edi), %eax
	movss	(%edi), %xmm3
	movl	%ebx, 36(%edi)
	movl	%eax, 4(%edi)
	movl	40(%edi), %eax
	movl	%ecx, 40(%edi)
	movl	%eax, 8(%edi)
	movl	44(%edi), %eax
	movl	%edx, 44(%edi)
	movl	%eax, 12(%edi)
	.p2align 4,,7
.L2:
	movss	20(%edi), %xmm1
	movaps	%xmm1, %xmm0
	andps	%xmm2, %xmm0
	comiss	%xmm4, %xmm0
	jae	.L7
	movl	16(%edi), %esi
	movl	%esi, -72(%ebp)
	movl	20(%edi), %ebx
	movl	%ebx, -68(%ebp)
	movl	24(%edi), %ecx
	movl	%ecx, -64(%ebp)
	movl	28(%edi), %edx
	movl	%edx, -60(%ebp)
	movl	32(%edi), %eax
	movl	%esi, 32(%edi)
	movl	%eax, 16(%edi)
	movl	36(%edi), %eax
	movl	%ebx, 36(%edi)
	movl	%eax, 20(%edi)
	movl	40(%edi), %eax
	movss	20(%edi), %xmm1
	movl	%ecx, 40(%edi)
	movl	%eax, 24(%edi)
	movl	44(%edi), %eax
	movl	%edx, 44(%edi)
	movl	%eax, 28(%edi)
.L7:
	movss	.LC2, %xmm0
	divss	%xmm3, %xmm0
	fld1
	movss	12(%edi), %xmm5
	movss	28(%edi), %xmm4
	movss	24(%edi), %xmm6
	movss	36(%edi), %xmm2
	movss	40(%edi), %xmm3
	movaps	%xmm0, %xmm7
	mulss	16(%edi), %xmm7
	movss	%xmm0, -76(%ebp)
	movss	4(%edi), %xmm0
	movss	%xmm0, -80(%ebp)
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm1
	movss	%xmm1, 20(%edi)
	movss	%xmm1, -84(%ebp)
	movss	8(%edi), %xmm1
	movaps	%xmm1, %xmm0
	movss	%xmm1, -88(%ebp)
	mulss	%xmm7, %xmm0
	mulss	%xmm5, %xmm7
	subss	%xmm0, %xmm6
	subss	%xmm7, %xmm4
	movss	-76(%ebp), %xmm7
	mulss	32(%edi), %xmm7
	movss	-80(%ebp), %xmm0
	movss	%xmm6, 24(%edi)
	mulss	%xmm7, %xmm0
	movss	%xmm4, 28(%edi)
	subss	%xmm0, %xmm2
	movaps	%xmm1, %xmm0
	movss	44(%edi), %xmm1
	mulss	%xmm7, %xmm0
	mulss	%xmm5, %xmm7
	movss	%xmm2, 36(%edi)
	subss	%xmm0, %xmm3
	movss	.LC2, %xmm0
	divss	-84(%ebp), %xmm0
	subss	%xmm7, %xmm1
	movaps	%xmm0, %xmm7
	movss	%xmm0, -92(%ebp)
	mulss	%xmm2, %xmm7
	movaps	%xmm6, %xmm0
	mulss	%xmm7, %xmm0
	mulss	%xmm4, %xmm7
	subss	%xmm0, %xmm3
	subss	%xmm7, %xmm1
	movss	%xmm3, 40(%edi)
	movss	%xmm1, 44(%edi)
	fstps	-108(%ebp)
	movss	-108(%ebp), %xmm0
	divss	%xmm3, %xmm0
	mulss	%xmm1, %xmm0
	movss	-88(%ebp), %xmm1
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm1
	subss	%xmm6, %xmm4
	subss	%xmm1, %xmm5
	mulss	-92(%ebp), %xmm4
	movss	-80(%ebp), %xmm1
	mulss	%xmm4, %xmm1
	subss	%xmm1, %xmm5
	mulss	-76(%ebp), %xmm5
	movss	%xmm5, (%edi)
	movss	%xmm4, 4(%edi)
	movss	%xmm0, 8(%edi)
	addl	$108, %esp
	popl	%ebx
	popl	%esi
	popl	%edi
	leave
	ret
	.p2align 4,,7
.L4:
	movl	(%edi), %esi
	movl	%esi, -56(%ebp)
	movl	4(%edi), %ebx
	movl	%ebx, -52(%ebp)
	movl	8(%edi), %ecx
	movl	%ecx, -48(%ebp)
	movl	12(%edi), %edx
	movl	%edx, -44(%ebp)
	movl	16(%edi), %eax
	movl	%esi, 16(%edi)
	movl	%eax, (%edi)
	movl	20(%edi), %eax
	movss	(%edi), %xmm3
	movl	%ebx, 20(%edi)
	movl	%eax, 4(%edi)
	movl	24(%edi), %eax
	movl	%ecx, 24(%edi)
	movl	%eax, 8(%edi)
	movl	28(%edi), %eax
	movl	%edx, 28(%edi)
	movl	%eax, 12(%edi)
	jmp	.L2
	.size	gauss, .-gauss
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	953267991
	.align 4
.LC6:
	.long	1065353216
	.text
	.p2align 4,,15
.globl gauss_check
	.type	gauss_check, @function
gauss_check:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	subl	$108, %esp
	movl	8(%ebp), %edi
	movss	.LC4, %xmm1
	movss	.LC5, %xmm2
	movss	(%edi), %xmm0
	andps	%xmm1, %xmm0
	comiss	%xmm2, %xmm0
	jae	.L13
	movss	16(%edi), %xmm0
	andps	%xmm1, %xmm0
	comiss	%xmm2, %xmm0
	jb	.L28
	movss	32(%edi), %xmm0
	andps	%xmm1, %xmm0
	movl	$-1, %eax
	comiss	%xmm2, %xmm0
	jb	.L12
	movl	(%edi), %esi
	movl	%esi, -56(%ebp)
	movl	4(%edi), %ebx
	movl	%ebx, -52(%ebp)
	movl	8(%edi), %ecx
	movl	%ecx, -48(%ebp)
	movl	12(%edi), %edx
	movl	%edx, -44(%ebp)
	movl	16(%edi), %eax
	movl	%esi, 16(%edi)
	movl	%eax, (%edi)
	movl	20(%edi), %eax
	movl	%ebx, 20(%edi)
	movl	%eax, 4(%edi)
	movl	24(%edi), %eax
	movl	%ecx, 24(%edi)
	movl	%eax, 8(%edi)
	movl	28(%edi), %eax
	movl	%edx, 28(%edi)
	movl	%eax, 12(%edi)
	.p2align 4,,7
.L13:
	movss	20(%edi), %xmm0
	andps	%xmm1, %xmm0
	comiss	%xmm2, %xmm0
.L30:
	jb	.L29
.L21:
	movss	40(%edi), %xmm6
	movaps	%xmm6, %xmm0
	movl	$-1, %eax
	andps	%xmm1, %xmm0
	comiss	%xmm2, %xmm0
	jb	.L12
	movss	.LC6, %xmm0
	divss	(%edi), %xmm0
	fld1
	xorl	%eax, %eax
	movss	4(%edi), %xmm1
	movss	12(%edi), %xmm4
	movss	28(%edi), %xmm3
	movss	24(%edi), %xmm5
	movss	36(%edi), %xmm2
	movss	%xmm1, -80(%ebp)
	movaps	%xmm0, %xmm7
	mulss	16(%edi), %xmm7
	movss	%xmm0, -76(%ebp)
	movaps	%xmm1, %xmm0
	movss	20(%edi), %xmm1
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm1
	movss	8(%edi), %xmm0
	movss	%xmm0, -88(%ebp)
	mulss	%xmm7, %xmm0
	mulss	%xmm4, %xmm7
	movss	%xmm1, -84(%ebp)
	movss	%xmm1, 20(%edi)
	movss	44(%edi), %xmm1
	subss	%xmm0, %xmm5
	subss	%xmm7, %xmm3
	movss	-76(%ebp), %xmm7
	mulss	32(%edi), %xmm7
	movss	-80(%ebp), %xmm0
	movss	%xmm5, 24(%edi)
	mulss	%xmm7, %xmm0
	movss	%xmm3, 28(%edi)
	subss	%xmm0, %xmm2
	movss	-88(%ebp), %xmm0
	mulss	%xmm7, %xmm0
	mulss	%xmm4, %xmm7
	movss	%xmm2, 36(%edi)
	subss	%xmm0, %xmm6
	movss	.LC6, %xmm0
	divss	-84(%ebp), %xmm0
	subss	%xmm7, %xmm1
	movaps	%xmm0, %xmm7
	movss	%xmm0, -92(%ebp)
	mulss	%xmm2, %xmm7
	movaps	%xmm5, %xmm0
	mulss	%xmm7, %xmm0
	mulss	%xmm3, %xmm7
	subss	%xmm0, %xmm6
	subss	%xmm7, %xmm1
	movss	%xmm6, 40(%edi)
	movss	%xmm1, 44(%edi)
	fstps	-108(%ebp)
	movss	-108(%ebp), %xmm0
	divss	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movss	-88(%ebp), %xmm1
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm1
	subss	%xmm5, %xmm3
	subss	%xmm1, %xmm4
	mulss	-92(%ebp), %xmm3
	movss	-80(%ebp), %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm4
	mulss	-76(%ebp), %xmm4
	movss	%xmm4, (%edi)
	movss	%xmm3, 4(%edi)
	movss	%xmm0, 8(%edi)
.L12:
	addl	$108, %esp
	popl	%ebx
	popl	%esi
	popl	%edi
	leave
	ret
	.p2align 4,,7
.L29:
	movss	36(%edi), %xmm0
	andps	%xmm1, %xmm0
	movl	$-1, %eax
	comiss	%xmm2, %xmm0
	jb	.L12
	movl	16(%edi), %esi
	movl	%esi, -72(%ebp)
	movl	20(%edi), %ebx
	movl	%ebx, -68(%ebp)
	movl	24(%edi), %ecx
	movl	%ecx, -64(%ebp)
	movl	28(%edi), %edx
	movl	%edx, -60(%ebp)
	movl	32(%edi), %eax
	movl	%esi, 32(%edi)
	movl	%eax, 16(%edi)
	movl	36(%edi), %eax
	movl	%ebx, 36(%edi)
	movl	%eax, 20(%edi)
	movl	40(%edi), %eax
	movl	%ecx, 40(%edi)
	movl	%eax, 24(%edi)
	movl	44(%edi), %eax
	movl	%edx, 44(%edi)
	movl	%eax, 28(%edi)
	jmp	.L21
	.p2align 4,,7
.L28:
	movl	(%edi), %esi
	movl	%esi, -40(%ebp)
	movl	4(%edi), %ebx
	movl	%ebx, -36(%ebp)
	movl	8(%edi), %ecx
	movl	%ecx, -32(%ebp)
	movl	12(%edi), %edx
	movl	%edx, -28(%ebp)
	movl	32(%edi), %eax
	movss	20(%edi), %xmm0
	andps	%xmm1, %xmm0
	movl	%eax, (%edi)
	movl	36(%edi), %eax
	movl	%esi, 32(%edi)
	movl	%ebx, 36(%edi)
	comiss	%xmm2, %xmm0
	movl	%eax, 4(%edi)
	movl	40(%edi), %eax
	movl	%ecx, 40(%edi)
	movl	%eax, 8(%edi)
	movl	44(%edi), %eax
	movl	%edx, 44(%edi)
	movl	%eax, 12(%edi)
	jmp	.L30
	.size	gauss_check, .-gauss_check
	.section	.note.GNU-stack,"",@progbits
	.ident	"GCC: (GNU) 3.4.0"
