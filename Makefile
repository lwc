CFLAGS= -O2 -g -Wall -Wno-parentheses -Wno-unused-but-set-variable -Wno-maybe-uninitialized
LDFLAGS= -Wl,-z,defs -Wl,--as-needed -Wl,--no-undefined

objdir/lwc: objdir/cpp.o objdir/lex.o objdir/dcl.o objdir/cdb.o objdir/fdb.o objdir/misc.o\
		objdir/util.o objdir/hier.o objdir/rexpr.o objdir/icode.o objdir/textp.o\
		objdir/inames.o\
		objdir/fspace.o objdir/debugs.o objdir/output.o objdir/except.o objdir/preproc.o\
		objdir/templates.o objdir/breakexpr.o objdir/statement.o objdir/lwc_config.o\
		objdir/regexp.o objdir/main.o
	gcc objdir/*.o -o $@ $(LDFLAGS)
	@echo Done.

objdir/%.o: %.c global.h
	gcc -o $@ -c $< $(CFLAGS)

global.h: norm.h SYS.h config.h
	touch global.h

clean:
	rm -fv objdir/*.o
	rm -fv objdir/lwc 

distclean:
	rm -fv objdir/*.o objdir/lwc 
	find . -name .preprocfile | xargs rm -fv
	find . -name a.out | xargs rm -fv
	find . -name \*.o | xargs rm -fv
	find . -name \*.i | xargs rm -fv
	find . -name GCC.c | xargs rm -fv

